Đây là dự án dựa trên hướng dẫn dbt tại https://www.startdataengineering.com/post/dbt-data-build-tool-tutorial

### Chuẩn bị

1. [Docker](https://docs.docker.com/get-docker/) và [Docker compose](https://docs.docker.com/compose/install/)
2. [dbt](https://docs.getdbt.com/dbt-cli/installation/)
3. [pgcli](https://www.pgcli.com/install)
4. [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Clone git repo này và chạy kho dữ liệu với docker container

```bash
git clone https://gitlab.com/phamducmanh1452001/simple-dbt-project.git
docker compose up -d
```

## Chạy dbt 

```bash
export DBT_PROFILES_DIR=$(pwd)
cd sde_dbt_tutorial
dbt snapshot
dbt run
dbt test
dbt docs generate
dbt docs serve
```

Thêm các cập nhật vào bảng khách hàng nguồn, để minh họa cho chức năng chụp ảnh nhanh (snapshot).

```bash
pgcli -h localhost -U dbt -p 5432 -d dbt
# password is password1234
COPY warehouse.customers(customer_id, zipcode, city, state_code, datetime_created, datetime_updated) FROM '/input_data/customer_new.csv' DELIMITER ',' CSV HEADER;
\q
```

Chạy chức năng chụp ảnh nhanh (snapshot) và tạo lại các mô hình.

```
dbt snapshot
dbt run
```

Bạn có thể đăng nhập vào kho dữ liệu để xem các mô hình.

```bash
pgcli -h localhost -U dbt -p 5432 -d dbt
# password is password1234
select * from warehouse.customer_orders limit 3;
\q
```

## Dừng docker container

```bash
cd ..
docker compose down
```